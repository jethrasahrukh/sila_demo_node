const http = require('http');
const port = 3000;
const sila = require("sila-sdk");
sila.configure({ handle: "tf_stage", key: "a8540fa2fe2e7057128746f9b4e6cdbb80c4ace0cf84297b5e792e989d411b0b" });

sila.setEnvironment("sandbox");

var handle = 'shahrukh657.silamoney.eth';

const ethWallet = require('ethereumjs-wallet');
let addressData = ethWallet.generate();

const wallet = sila.generateWallet();

var ethereum_private_key = wallet.privateKey;
var publicToken = "public-sandbox-2dff51b9-55b5-43ed-addb-c4a38136f7b9";
console.log("wallet", wallet);

const server = http.createServer((req, res) => {
    switch (req.url) {
        case "/checkHandle":
            sila.checkHandle(handle).then(sres => {
                res.end(JSON.stringify(sres));
            })
                .catch(err => {
                    console.log("err", err);
                    res.end(JSON.stringify(err));
                });
            break;
        case "/register":
            var user = {
                handle: handle, // Required: Must not be already in use
                first_name: 'Dipak',        // Required
                last_name: 'Chaudhary',            // Required
                name: 'Example User',         // Optional: defaults to `first_name last_name`
                address: '145 Brooklyn Ave, Brooklyn, NY 11213, United States',      // Required: must be a valid USPS mailing address
                address_2: '',       // Optional: required if USPS requires it
                city: 'New York',             // Required: Must be a valid US City matching the zip
                state: 'NY',                  // Required: Must be a 2 character US State abbr.
                zip: '11213',            // Required: Must be a valid US Postal Code
                phone: '171-873-5440',        // Required: Must be a valid phone number (format not enforced)
                email: 'dip@email.com',      // Required: Must be a valid email address
                crypto: wallet.address,
                ssn: '569856985',
                dob: '1991-06-19'
            }
            sila.register(user)
                .then(sres => {
                    res.end(JSON.stringify(sres));
                    console.log("register Response", sres);

                })
                .catch(err => {
                    res.end(JSON.stringify(err));
                });
            break;

        case "/requestKYC":
            var userkyc = {
                handle: handle
            }
            sila.requestKYC(userkyc, ethereum_private_key)
                .then(sres => {
                    res.end(JSON.stringify(sres));
                })
                .catch(err => {
                    res.end(JSON.stringify(err));
                });
            break;
        case "/checkKYC":
            sila.checkKYC(handle, ethereum_private_key)
                .then(sres => { res.end(JSON.stringify(sres)); })
                .catch(err => { res.end(JSON.stringify(err)); });
            break;
        case "/getAccounts":
            sila.getAccounts(handle, ethereum_private_key)
                .then(sres => {
                    res.end(JSON.stringify(sres));
                })
                .catch(err => {
                    res.end(JSON.stringify(err));
                });
        case "/linkAccount":
            sila.linkAccount(handle, ethereum_private_key, publicToken)
                .then(res => { /* Handle response res */ })
                .catch(err => { /* Handle errors err */ });
        case "/getAccounts":
            sila.getAccounts(handle, ethereum_private_key)
                .then((res) => { /* Handle response res */ })
                .catch((err) => { /* Handle error `err` */ });
            break;
    }




});
server.listen(port, () => {
    console.log(`Server running.....`);
});